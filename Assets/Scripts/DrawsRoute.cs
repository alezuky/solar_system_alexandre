﻿using UnityEngine;
using System.Collections;



public class DrawsRoute : MonoBehaviour {

    //Uses this bool to know when to draw the dots or not
    [HideInInspector]
    public bool draws;

    private bool parametersSet;

    //Line parameters variables
    private LineRenderer line;
    [HideInInspector]
    public float width = 0f;

    //Resolution of the line in points / second of the translation time
    public float resolution = 1;
    public int length = 50;
    int i = 0;
    //[HideInInspector]
    public float timeforTranslation;

    private float counter = 0f;
    private float deltatranslation = 0f;

    

	// Use this for initialization
	void Start () {

        line = gameObject.GetComponent<LineRenderer>();

        draws = true;
        parametersSet = false;
        
      }
	
	// Update is called once per frame
	void Update () {

        counter += Time.deltaTime;

        //Set the parameters after all the Start() functions are called
        if (counter > 0.5f && parametersSet == false)
        {            
            SetLineParameters();
            parametersSet = true;            
        }

        if (draws ==  true && parametersSet == true)
        {
            Draws();            
        }
        else
        {
            Hides();
        }
	
	}

    void Draws()
    {
        line.enabled = true;
        if (i < length)
        {
            if (counter > deltatranslation)
            {
                Vector3 pos = transform.position;
                line.SetVertexCount(i+1);
                line.SetPosition(i, pos);
                i++;
                counter = 0;
            }
        }
    }

    private void Hides()
    {

        line.enabled = false;

    }


    public void Erases()
    {
        line.SetVertexCount(0);
        i = 0;
        counter = 0;
        parametersSet = true;
    }

    void SetLineParameters()
    {
        length = (int)(timeforTranslation * resolution);
        line.SetWidth(width, width);
        deltatranslation = timeforTranslation / length;
    }

}
