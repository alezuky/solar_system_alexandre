﻿//This class is used for the Rotation Movement of Planets

using UnityEngine;
using System.Collections;

public class Rotates : MonoBehaviour {

    //Rotation Speed
    [HideInInspector]
    public float rotationSpeed;

	private float timer = 0f;

	void Update () {

		//Clamp our timer
		float completeRotationTime = 360f / rotationSpeed;
		timer += Time.deltaTime;
		if (timer > completeRotationTime)
			timer = 0f;

		//t is the percentage of time that has already passed
		float t = timer / completeRotationTime;

		//Learp the euler angles of our objects to rotate it
		float angle = Mathf.Lerp(0f, 360f, t);
		transform.eulerAngles = new Vector3(0f, angle, 0f);

	}
}
