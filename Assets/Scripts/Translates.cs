﻿//This class is used for the Translation Movement of Planes

using UnityEngine;
using System.Collections;

public class Translates : MonoBehaviour {

    //Ellipse Parameters
    private float a;
    [HideInInspector]
    public float b;    
    private int quadrant = 2;

    //Translation Speed
    //[HideInInspector]
    public float translationSpeed;

    private float timer = 0f;

    //The perimeter of the elipse
    //[HideInInspector]
    public float perimeter;

  
    // Use this for initialization
    void Start () {

        //Estimating an orbit
        a = b * 1.2f;

        //Calculates our perimeter
        perimeter = 2 * Mathf.PI * (Mathf.Sqrt((Mathf.Pow(a, 2) + Mathf.Pow(b, 2)) / 2));
	}
	

	void Update () {

        //Clamp our timer for each quadrant of the ellipse
        float quadrantTime = (perimeter/4) / translationSpeed;
        timer += Time.deltaTime;
        if (timer > quadrantTime)
        {
            timer = 0f;
            quadrant++;
            if (quadrant > 4)
                quadrant = 1;
        }

        //The x of our ellpise is the percentage of time that has already passed
        float x = (timer / quadrantTime)*a;
        //In quadrants 1 and 3, our x goes backwards
        if (quadrant == 1 || quadrant == 3)
            x = a - x;

        //The ellipse formula, isolating z and for each quadrant
        float z = Mathf.Sqrt(   (1 - (Mathf.Pow(x, 2) / Mathf.Pow(a, 2)) ) * Mathf.Pow(b, 2)   );

        //We get x and z values as percentages of their max values to use them in learp
        float xRel = x / a;
        float zRel = z / b;

        float xPos = Mathf.Lerp(0, a, xRel);
        float zPos = Mathf.Lerp(0, b, zRel);

        //In quadrands 2 and 3, our x is negative
        if (quadrant == 2 || quadrant == 3)
            xPos = -xPos;

        //In quadrands 3 and 4, our z is negative
        if (quadrant == 3 || quadrant == 4)
            zPos = -zPos;

        //Translates our planet
        transform.position = new Vector3(xPos, 0, zPos);         
        
    }
}
