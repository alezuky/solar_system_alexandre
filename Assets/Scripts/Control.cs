﻿//This Class controls the game variables

using UnityEngine;
using System.Collections;


public class Control : MonoBehaviour {

    private GameObject Sun;
    private GameObject Mercury;
    private GameObject Venus;
    private GameObject Earth;
    private GameObject Mars;
    private GameObject Jupiter;
    private GameObject Saturn;
    private GameObject Uranus;
    private GameObject Neptune;
    private GameObject Pluto;

    private GameObject[] planets = new GameObject[9];

    //References
    Vector3 referenceScale = new Vector3();
    float referenceTranslation = 0f;
    float referenceRotation = 0f;

    //Public settings
    public float EarthSize = 1f;
    public float EarthRotationSpeed = 1f;



    void Awake () {

        Sun = GameObject.Find("Sun");
        Mercury = GameObject.Find("Mercury");
        Venus = GameObject.Find("Venus");
        Earth = GameObject.Find("Earth");
        Mars = GameObject.Find("Mars");
        Jupiter = GameObject.Find("Jupiter");
        Saturn = GameObject.Find("Saturn");
        Uranus = GameObject.Find("Uranus");
        Neptune = GameObject.Find("Neptune");
        Pluto = GameObject.Find("Pluto");

        planets[0] = Mercury;
        planets[1] = Venus;
        planets[2] = Earth;
        planets[3] = Mars;
        planets[4] = Jupiter;
        planets[5] = Saturn;
        planets[6] = Uranus;
        planets[7] = Neptune;
        planets[8] = Pluto;

        SetAllParameters();
    }

    void Start()
    {

    }
	
	void Update() {


        //Real time resize
        if (EarthSize != Earth.transform.localScale.x || EarthRotationSpeed != Earth.GetComponent<Rotates>().rotationSpeed)
        {            
            ErasesRoutes();
            SetAllParameters();            
        }
        //Set the Line Parameters of the Planets' Routes
        SetRoutesParameters();

    }

    void SetParameters (GameObject planet, float translation, float scale, float rotation)
    {
        planet.transform.localScale = referenceScale*scale;
        planet.GetComponent<Rotates>().rotationSpeed = referenceRotation * rotation;
        planet.GetComponent<Translates>().translationSpeed = referenceTranslation * translation;
                
    }

    void SetParameters(GameObject planet, float scale)
    {
        planet.transform.localScale = referenceScale * scale;
    }

    void SetDistance(GameObject planet, GameObject refPlanet)
    {
        //All the planets will have 5x the size of the Earth between each of them and the next planet
        float distance = referenceScale.x*5;

        float sizePlanet = planet.transform.localScale.x;
        float sizeRefPlanet = refPlanet.transform.localScale.x;

        float sum = 0;
        if (refPlanet.name == "Sun") sum = sizePlanet / 2 + sizeRefPlanet * 0.7f + distance;
        else sum = sizePlanet / 2 + sizeRefPlanet / 2 + distance + refPlanet.GetComponent<Translates>().b;

        //transfer the found value to the orbit of the planet
        planet.GetComponent<Translates>().b =  sum;
          

    }

    void ErasesRoutes ()
    {
        foreach (GameObject planet in planets)
        {
            planet.GetComponent<DrawsRoute>().Erases();
        }
    }

    void SetRoutesParameters ()
    {
        foreach (GameObject planet in planets)
        {
            planet.GetComponent<DrawsRoute>().timeforTranslation = planet.GetComponent<Translates>().perimeter / planet.GetComponent<Translates>().translationSpeed;
            planet.GetComponent<DrawsRoute>().width = planet.transform.localScale.x / 2;
        }
        
    }

    void SetAllParameters()
    {
        //Earth Parameters
        Earth.transform.localScale = new Vector3(EarthSize, EarthSize, EarthSize);
        Earth.GetComponent<Rotates>().rotationSpeed = EarthRotationSpeed;
        //Earth Rotation is relative to its Translation
        Earth.GetComponent<Translates>().translationSpeed = Earth.GetComponent<Rotates>().rotationSpeed / 365f;

        //Set the reference -- It is another variable just in case we decide not to make relative to Earth
        referenceScale = Earth.transform.localScale;
        referenceTranslation = Earth.GetComponent<Translates>().translationSpeed;
        referenceRotation = Earth.GetComponent<Rotates>().rotationSpeed;

        //Set the other parameters
        //Considering Mean Orbit = Translation Speed and Sideral Rotation = Rotation Speed    
        SetParameters(Sun, 109.2f);
        SetParameters(Mercury, 1.590f, 0.3829f, 1 / 58.81f);
        SetParameters(Venus, 1.176f, 0.9499f, 1 / 243.68f);
        SetParameters(Mars, 0.808f, 0.5320f, 23.934f / 24.623f);
        SetParameters(Jupiter, 0.438f, 10.9733f, 1 / 0.41467f);
        SetParameters(Saturn, 0.324f, 9.1402f, 1 / 0.445f);
        SetParameters(Uranus, 0.228f, 3.9809f, 1 / -0.718f);
        SetParameters(Neptune, 0.182f, 3.8647f, 1 / 0.67f);
        SetParameters(Pluto, 0.157f, 0.1807f, 1 / -6.387f);

        //Set distances between planets
        SetDistance(Mercury, Sun);
        SetDistance(Venus, Mercury);
        SetDistance(Earth, Venus);
        SetDistance(Mars, Earth);
        SetDistance(Jupiter, Mars);
        SetDistance(Saturn, Jupiter);
        SetDistance(Neptune, Saturn);
        SetDistance(Uranus, Neptune);
        SetDistance(Pluto, Uranus);

        }
}
