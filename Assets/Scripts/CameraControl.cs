﻿//This class controls the camera

using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    private GameObject Sun;
    private GameObject Mercury;
    private GameObject Venus;
    private GameObject Earth;
    private GameObject Mars;
    private GameObject Jupiter;
    private GameObject Saturn;
    private GameObject Uranus;
    private GameObject Neptune;
    private GameObject Pluto;

    private GameObject[] planets = new GameObject[9];

    //References
    Vector3 referenceScale = new Vector3();
    

    GameObject target;
    bool notarget = true;

    void Start()
    {

        Sun = GameObject.Find("Sun");
        Mercury = GameObject.Find("Mercury");
        Venus = GameObject.Find("Venus");
        Earth = GameObject.Find("Earth");
        Mars = GameObject.Find("Mars");
        Jupiter = GameObject.Find("Jupiter");
        Saturn = GameObject.Find("Saturn");
        Uranus = GameObject.Find("Uranus");
        Neptune = GameObject.Find("Neptune");
        Pluto = GameObject.Find("Pluto");

        
        planets[0] = Mercury;
        planets[1] = Venus;
        planets[2] = Earth;
        planets[3] = Mars;
        planets[4] = Jupiter;
        planets[5] = Saturn;
        planets[6] = Uranus;
        planets[7] = Neptune;
        planets[8] = Pluto;


        //Set the reference
        referenceScale = Earth.transform.localScale;

        target = Sun;

    }

    void Update()
    {
        

        if (Input.GetKey("[0]"))
        {
            target = Sun;
            notarget = false;
        }else if (Input.GetKey("[1]"))
        {
            target = Mercury;
            notarget = false;
        }
        else if (Input.GetKey("[2]"))
        {
            target = Venus;
            notarget = false;
        }
        else if (Input.GetKey("[3]"))
        {
            target = Earth;
            notarget = false;
        }
        else if (Input.GetKey("[4]"))
        {
            target = Mars;
            notarget = false;
        }
        else if (Input.GetKey("[5]"))
        {
            target = Jupiter;
            notarget = false;
        }
        else if (Input.GetKey("[6]"))
        {
            target = Saturn;
            notarget = false;
        }
        else if (Input.GetKey("[7]"))
        {
            target = Uranus;
            notarget = false;
        }
        else if (Input.GetKey("[8]"))
        {
            target = Neptune;
            notarget = false;
        }
        else if (Input.GetKey("[9]"))
        {
            target = Pluto;
            notarget = false;
        }
        else if (notarget == true)
        {
            //Inition Position / Rotation of the Camera for overview
            transform.position = new Vector3(0, referenceScale.x * 150, 0);
            transform.eulerAngles = new Vector3(90, 0, 90);
            gameObject.GetComponent<Camera>().nearClipPlane = 20f;
            gameObject.GetComponent<Camera>().farClipPlane = 200f;
        }

        if (notarget == false) LookAtPlanet(target);

        //SpaceBar resets our camera
        if (Input.GetKey("space"))
        {
            notarget = true;
            AddsRouts();
        }
    }

    void LookAtPlanet(GameObject planet)
    {
        RemoveRouts();

        transform.LookAt(planet.transform);
        transform.position = new Vector3(planet.transform.position.x -planet.transform.localScale.x, planet.transform.position.y, planet.transform.position.z -planet.transform.localScale.z);

        //Adjusting the camera for each planet
        if (planet.name == "Pluto")
        {
            gameObject.GetComponent<Camera>().nearClipPlane = 0.1f;
            gameObject.GetComponent<Camera>().farClipPlane = 80f;
        }
        else if (planet.name == "Mercury")
        {
            gameObject.GetComponent<Camera>().nearClipPlane = 0.1f;
            gameObject.GetComponent<Camera>().farClipPlane = 20f;
        }
        else if (planet.name == "Sun")
        {
            gameObject.GetComponent<Camera>().nearClipPlane = 0.3f;
            gameObject.GetComponent<Camera>().farClipPlane = 250f;
        }
        else
        {
            gameObject.GetComponent<Camera>().nearClipPlane = 0.3f;
            gameObject.GetComponent<Camera>().farClipPlane = 15.0f;
        }
        
    }

    void RemoveRouts ()
    {
        foreach (GameObject planet in planets)
        {
            planet.GetComponent<DrawsRoute>().draws = false;
        }
    }

    void AddsRouts()
    {
        foreach (GameObject planet in planets)
        {
            planet.GetComponent<DrawsRoute>().draws = true;
        }
    }


}
